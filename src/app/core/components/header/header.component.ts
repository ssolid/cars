import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import Cars from '../../models/ICars';
import { DataService } from '../../../common/service/data/data.service';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass']
})
export class HeaderComponent implements OnInit {

  constructor(private modalService: NgbModal, private dataService: DataService) {
  }

  selectedCars: Cars[] = [];
  modalReference: NgbModalRef;

  ngOnInit() {
  }

  countFavourite() {
      return this.dataService.cars.filter(c => c.isFavourite).length;
  }

  openModal(content) {
    this.selectedCars  =  this.dataService.cars.filter(c => c.isFavourite);
    this.modalReference = this.modalService.open(content, { centered: true });
  }

  removeFromFavourite(car: Cars) {
    this.dataService.cars.find(c => c.id === car.id).isFavourite = false;
    this.selectedCars  =  this.dataService.cars.filter(c => c.isFavourite);
    if(this.selectedCars.length === 0) {
      this.modalReference.close();
    }
  }

}
