import { Component, OnInit, ViewChild } from '@angular/core';
import { DataService } from '../../../common/service/data/data.service';
import Cars from '../../models/ICars';
import {NgbTypeahead} from '@ng-bootstrap/ng-bootstrap';
import {debounceTime, distinctUntilChanged, filter, map} from 'rxjs/operators';
import {Observable, Subject, merge} from 'rxjs';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {

  cars: Cars[];
  initialCars: Cars[];
  brands: any[] = [];
  rangePrice = [0, 100000];
  showResult = false;

  modelCar: string = '';
  @ViewChild('instance') instance: NgbTypeahead;
  focus$ = new Subject<string>();
  click$ = new Subject<string>();
  models: string[] = [];

  constructor(private dataService: DataService) {
  }

  ngOnInit() {
    this.dataService.getData().subscribe((data: Array<Cars>) => {
      this.initialCars = data;
      this.brands = this.dataService.getBrands(data);
      this.models = this.dataService.getModels(data);
    });


  }


  search = (text$: Observable<string>) => {
    const debouncedText$ = text$.pipe(debounceTime(200), distinctUntilChanged());
    return merge(debouncedText$).pipe(
      map(term => (term === '' ? this.models
        : this.models.filter(v => v.toLowerCase().indexOf(term.toLowerCase()) > -1)).slice(0, 10))
    );
  }

  addFavourite(car) {
    this.dataService.add(car);
  }

  applySearchBrand(brand) {
    brand.isSelected = !brand.isSelected;
  }

  getBrandsSelected() {
    return this.brands.filter(b => b.isSelected).map(b => b['name'].toLowerCase());
  }

  refreshResults() {
    const brandSelected = this.getBrandsSelected();
    this.cars = this.initialCars

    // Brand
    if(brandSelected.length > 0){
      this.cars = this.cars.filter(c => {
        return brandSelected.indexOf(c.brand.toLowerCase()) > -1;
      });
    }

    // Model
    if(this.modelCar != '') {
      this.cars = this.cars.filter(c => c.model.toLowerCase() === this.modelCar.toLowerCase());
    }

    // price
    this.cars = this.cars.filter(c => Number(c.price) >= this.rangePrice[0] && Number(c.price) <= this.rangePrice[1]);

  }

  applySearch(){
    this.showResult = true;
    this.refreshResults();
  }
}
