export default interface Cars {
    brand: String;
    id: String;
    image: String;
    model: String;
    price: String;
    isFavourite: boolean;
  }
