import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import Cars from '../../../core/models/ICars';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { forEach } from '@angular/router/src/utils/collection';
import { Subject, Observable } from 'rxjs/index';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  constructor(private httpClient: HttpClient) {
  }

  public cars: Cars[] = [];

  getData(): Observable<Cars[]> {
    return this.httpClient.get<Cars[]>('./assets/data/cars.json').pipe(
      map(res => {
        res.forEach(c => {
          if (!c.hasOwnProperty('isFavourite')) {
            c['isFavourite'] = false;
          }
        });
        this.cars = res;
        return res;
      })
    );
  }

  add(car: Cars) {
    this.cars.forEach(c => {
      if (c.id === car.id) {
        c.isFavourite = true;
      }
    });
  }

  getCars() {
    return this.cars;
  }

  getBrands(cars) {
    let result = [];
    result = cars.map(car => car.brand)
    .filter((v, i, a) => a.indexOf(v) === i) // remove doublon
    .map(b => {
      return {
        name : b,
        isSelected: false
      };
    });
    return result;
  }

  getModels(cars) {
    return cars.map(car => car.model)
    .filter((v, i, a) => a.indexOf(v) === i) // remove doublon
  }

}
